import {Rarity} from '../common/constants';
import {Effect, EffectTemplate} from '../effects/effect';

export interface IngredientTemplate {
  name: string;
  rarity: Rarity;
  quantity?: number;
  effects?: EffectTemplate[];
}

export class Ingredient implements IngredientTemplate {

  public name: string;
  public rarity: Rarity;
  public quantity: number;
  public effects: Effect[];

  constructor(
    template: IngredientTemplate
  ) {
    this.quantity = 1;
    this.effects = [];
    Object.assign(this, template);
    this.realizeEffects();
  }

  public toString(): string {
    return `${this.quantity}x ${this.name}`;
  }

  public dup(): IngredientTemplate {
    return new Ingredient(this);
  }

  public learnEverything(): void {
    for (const effect of this.effects) {
      effect.learn();
    }
  }

  protected realizeEffects(): void {
    if (this.effects.length === 0) { return; }
    const initial = this.effects[0];
    // Here we look for a specific function that the Effect class implements
    // but which the template does not.  If it's there, these are already
    // first-class Effects.
    if (initial.dup) { return; }
    this.effects = this.effects.map(template => {
      return new Effect(template);
    });
  }
}

export class IngredientBag {
  ingredients: Ingredient[];

  private ingredientsByName: {[key: string]: Ingredient};

  constructor() {
    this.clear();
  }

  public add(...templates: IngredientTemplate[]): void {
    for (const template of templates) {
      const preexisting = this.ingredientsByName[template.name];
      if (!preexisting) {
        const newIngredient = new Ingredient(template);
        this.ingredientsByName[template.name] = newIngredient;
        this.ingredients.push(newIngredient);
      } else {
        preexisting.quantity += template.quantity;
      }
    }
  }

  public has(ingredient: Ingredient): boolean {
    return (this.ingredientsByName[ingredient.name] != null);
  }

  public addOne(ingredient: Ingredient): void {
    const middle = ingredient.dup();
    middle.quantity = 1;
    this.add(middle);
  }

  public removeOne(ingredient: Ingredient): boolean {
    const preexisting = this.ingredientsByName[ingredient.name];
    if (!preexisting) { return false; }
    preexisting.quantity -= 1;
    if (preexisting.quantity <= 0) {
      this.ingredientsByName[ingredient.name] = null;
      const index = this.ingredients.findIndex(ingr => ingr.name === ingredient.name);
      if (index === -1) {
        console.error("Should never happen: couldn't find", ingredient, "in", this.ingredients);
        return false;
      }
      this.ingredients.splice(index, 1);
    }
    return true;
  }

  public numHolding(): number {
    return this.ingredients.reduce(((acc, cur) => acc + cur.quantity), 0);
  }

  public isEmpty(): boolean {
    return this.ingredients.length === 0;
  }

  public clear(): void {
    this.ingredients = [];
    this.ingredientsByName = {};
  }
}

export class Potion extends Ingredient {

  static readonly PLACEHOLDER = "PLACEHOLDER";

  constructor(props) {
    super(props);
    if (this.name === Potion.PLACEHOLDER) {
      this.name = this.displayName();
    }
  }

  public displayName(): string {
    let maxMag = 0;
    let prominent: Effect = null;
    for (let effect of this.effects) {
      if (effect.magnitude() > maxMag) {
        maxMag = effect.magnitude();
        prominent = effect;
      }
    }

    if (prominent) {
      return `Potion of ${prominent.displayName()}`;
    } else {
      return "Potion of nothing much";
    }
  }

}
