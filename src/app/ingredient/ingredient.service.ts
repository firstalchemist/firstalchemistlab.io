import {Injectable} from '@angular/core';
import {Ingredient} from './ingredient';
import {ALL_INGREDIENTS, IngredientCategory} from './all_ingredients';
import {Effect} from '../effects/effect';
import {WeightedList} from '../common/weighted_list';
import {ALL_EFFECTS, EffectCategory} from '../effects/all_effects';
import {Rarity} from '../common/constants';
import {randomBetween} from '../common/utils';
import {MessageService} from '../message/message.service';
import {PlayerService} from '../player/player.service';


@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  protected allIngredients: Ingredient[];
  protected weightedIngredients: WeightedList<Ingredient>;

  protected allEffects: Effect[];


  constructor(
    public messageService: MessageService,
    public playerService: PlayerService,
  ) {
    this.allIngredients = [];
    this.weightedIngredients = new WeightedList();
    this.generateIngredients();
    // for debugging purposes
    window['ingredientService'] = this;
  }

  public gatherIngredients(numRolls = 3): Ingredient[] {
    const result = [];
    for (let i = 0; i < numRolls; i++) {
      result.push(this.weightedIngredients.atRandom());
    }
    this.messageService.add("You gathered " + result.join(","));
    return result;
  }

  protected generateIngredients(): void {
    if (this.fetchGeneratedIngredients()) { return; }
    // Grass and wheat are two ingredients that are always the same
    const grass = new Ingredient({
      name: "Grass",
      rarity: Rarity.COMMON,
    });
    const wheat = new Ingredient({
      name: "Wheat",
      rarity: Rarity.UNCOMMON, // for now
    });
    const wheatgrass = new Ingredient({
      name: "Wheatgrass",
      rarity: Rarity.RARE,  // for now
    });
    this.allIngredients.push(grass, wheat, wheatgrass);

    for (const category of ALL_INGREDIENTS) {
      this.allIngredients.push(...this.ingredientsFromCategory(category));
    }

    const effects = new WeightedList<Effect>();
    for (const category of ALL_EFFECTS) {
      effects.add(...this.effectsFromCategory(category));
    }

    for (const ingredient of this.allIngredients) {
      this.weightedIngredients.add(ingredient);
      // For now we're just going to give each ingredient three effects
      for (let i = 0; i < 3; i++) {
        const effect = effects.atRandom().dup() as Effect;
        effect.power = randomBetween(-0.3, 0.3);
        ingredient.effects.push(effect);
      }
    }
    this.playerService.saveIngredientsAndEffects(this.allIngredients);
  }

  protected fetchGeneratedIngredients(): boolean {
    const fetched = this.playerService.loadIngredientsAndEffects();
    if (!fetched) { return null; }
    this.allIngredients = fetched;
    this.weightedIngredients.add(...this.allIngredients);
    return true;
  }

  protected ingredientsFromCategory(category: IngredientCategory): Ingredient[] {
    const result: Ingredient[] = [];

    for (const member of category.members) {
      const parts = [...category.parts];
      const removes = member.removes || [];
      for (const remove of removes) {
        const index = parts.indexOf(remove);
        if (index === -1) {
          console.warn("Could not remove", remove, "from", parts);
        } else {
          parts.splice(index, 1);
        }
      }
      const adds = member.adds || [];
      parts.push(...adds);

      for (const part of parts) {
        const ingredient = new Ingredient({
          name: `${member.name} ${part}`,
          rarity: Rarity.COMMON, // for now
        });
        result.push(ingredient);
      }
    }

    return result;
  }

  protected effectsFromCategory(category: EffectCategory): Effect[] {
    const result: Effect[] = [];
    for (const member of category.members) {
      const effect = new Effect({
        name: `${category.positiveTemplate} ${member.name}`,
        negative: `${category.negativeTemplate} ${member.name}`,
        rarity: category.rarity,
        salesCategories: category.salesCategories,
      });
      result.push(effect);
    }
    return result;
  }


}
