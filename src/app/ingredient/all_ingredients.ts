
/*
{
	category: "Plants",
	prefixParts: [ "whole" ],
	parts: [
		"roots", "stem", "seeds"
	],
	members: [
		{name: "Garlic", adds: ["cloves"]},
		{name: "Mushroom", removes: ["roots", "seeds"], adds: ["cap"]},

	]
}
*/

export interface CategoryMember {
  name: string;
  adds?: string[];
  removes?: string[];
}

export interface IngredientCategory {
  category: string;
  prefixParts?: string[];
  parts: string[];
  members: CategoryMember[];
}

export const ALL_INGREDIENTS: IngredientCategory[] = [
  {
    category: "Plants",
    parts: [
      "roots", "stem", "seeds"
    ],
    members: [
      {name: "Garlic", adds: ["cloves"], removes: ["roots", "seeds"]},
      {name: "Mushroom", adds: ["cap"], removes: ["roots", "seeds"]},
      {name: "Grape"}
    ]
  }
];
