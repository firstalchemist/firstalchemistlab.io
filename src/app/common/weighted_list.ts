
export interface HasRarity {
  rarity: number;
}

export class WeightedList<T extends HasRarity> {
  protected totalWeight: number;
  protected weights: [number, T][];

  constructor() {
    this.totalWeight = 0;
    this.weights = [];
  }

  add(...items: T[]) {
    for (const item of items) {
      this.totalWeight += item.rarity;
      this.weights.push([this.totalWeight, item]);
    }
  }

  atRandom(): T {
    const roll = Math.floor(Math.random() * this.totalWeight);
    for (const [weight, item] of this.weights) {
      if (roll < weight) {
        return item;
      }
    }
    console.warn("Should never happen!  Rolled", roll, "of", this.weights);
  }
}
