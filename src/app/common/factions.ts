import {Potion} from '../ingredient/ingredient';
import {Effect, SalesCategory} from '../effects/effect';

export class Faction {
  constructor(
    public name: string,
    public description: string,
    public prefers?: SalesCategory,
    public dislikes?: SalesCategory,
  ) {

  }

  public priceFor(potion: Potion): number {
    let total = 0;
    for (const effect of potion.effects) {
      total += this.priceForEffect(effect);
    }
    return total;
  }

  public priceForEffect(effect: Effect): number {
    const basePrice = effect.basePrice();
    if (!this.prefers) { return basePrice; }
    const effectCategory = effect.salesCategory();
    let mult = 1;
    if (effectCategory === this.prefers) {
      mult += 0.5;
    }
    if (effectCategory === this.dislikes) {
      mult -= 0.5;
    }
    return basePrice * mult;
  }
}

/*
Military likes buffs.  Anything defensive, boosting, resistance, status effects, etc.
 Has little use for poisons and esoterics.  Low tolerance for toxicity.
Nobles like esoterics, and buffs to MIND, CHA and LUCK.  Some use for poisons. Some tolerance for toxicity.
Underworld likes poisons.  Some buffs (DEX, CHA) and esoterics (invis).  Little use for the rest of it.  High tolerance for toxicity.
Wizards have randomly chosen (and randomly changing) preferences, based on what they're researching at any given time.  They are the only group that doesn't care about side effects or toxicity at all (they're not taking the potions, just studying them)
Merchants will buy absolutely anything, but only at market rate.  That's what makes it the market rate.  Doubles low instability bonus, has some tolerance for toxicity.
*/

export const ALL_FACTIONS = [
  new Faction("Military",
    "Prefers boosts, dislikes poisons",
    SalesCategory.BOOST, SalesCategory.POISON
  ),
  new Faction("Nobles",
    "Prefer esoterics, dislikes boosts",
    SalesCategory.ESOTERIC, SalesCategory.BOOST
    ),
  new Faction("Underworld",
    "Prefer poisons, dislikes esoterics",
    SalesCategory.POISON, SalesCategory.ESOTERIC),
  new Faction("Wizards", "Will buy anything (for research), but are cheap"),
];
