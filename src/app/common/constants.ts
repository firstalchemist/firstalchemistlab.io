export enum Rarity {
  COMMON = 80,
  UNCOMMON = 30,
  RARE = 3,
}

export const PRICE_BY_RARITY = {
  [Rarity.COMMON]: 10,
  [Rarity.UNCOMMON]: 25,
  [Rarity.RARE]: 200
};
