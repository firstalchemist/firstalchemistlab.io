import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { CauldronComponent } from './cauldron/cauldron.component';
import { InventoryComponent } from './inventory/inventory.component';
import { IngredientComponent } from './ingredient/ingredient.component';
import { MessageComponent } from './message/message.component';
import { MarketplaceComponent } from './marketplace/marketplace.component';
import { PotionComponent } from './potion/potion.component';
import { PlayerComponent } from './player/player.component';

@NgModule({
  declarations: [
    AppComponent,
    CauldronComponent,
    InventoryComponent,
    IngredientComponent,
    MessageComponent,
    MarketplaceComponent,
    PotionComponent,
    PlayerComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
