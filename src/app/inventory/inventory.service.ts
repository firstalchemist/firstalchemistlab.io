import { Injectable } from "@angular/core";
import {Ingredient, IngredientBag, Potion} from '../ingredient/ingredient';
import {IngredientService} from '../ingredient/ingredient.service';
import {PlayerService} from '../player/player.service';

export interface SavedInventory {
  ingredients: Ingredient[];
  cauldron: Ingredient[];
  potions: Ingredient[];
}

@Injectable({
  providedIn: "root"
})
export class InventoryService {

  ingredients: IngredientBag;
  cauldronContents: IngredientBag;
  // Specifically not an ingredient bag, as potions aren't unique by name
  potions: Potion[];

  constructor(
    public ingredientService: IngredientService,
    public playerService: PlayerService,
  ) {
    this.ingredients = new IngredientBag();
    this.cauldronContents = new IngredientBag();
    this.potions = [];
    this.restoreFromSavedInventory();
  }

  public gatherIngredients(): void {
    this.playerService.advanceTime(2);
    const gathered = this.ingredientService.gatherIngredients();
    this.ingredients.add(...gathered);
    this.updateSavedInventory();
  }

  public cauldronHasRoomFor(ingredient: Ingredient): boolean {
    if (this.cauldronContents.numHolding() >= 2) { return false; }
    if (this.cauldronContents.has(ingredient)) { return false; }
    return true;
  }

  public addIngredientToCauldron(ingredient: Ingredient): boolean {
    return this.transferItem(ingredient, this.ingredients, this.cauldronContents);
  }

  public removeIngredientFromCauldron(ingredient: Ingredient): boolean {
    return this.transferItem(ingredient, this.cauldronContents, this.ingredients);
  }

  public addPotion(potion: Potion): void {
    this.potions.push(potion);
    this.updateSavedInventory();
  }

  public removePotion(potion: Potion): void {
    const index = this.potions.findIndex(pot => pot === potion);
    if (index === -1) {
      console.error("Should never happen: couldn't find", potion, "in", this.potions);
      return;
    }
    this.potions.splice(index, 1);
    this.updateSavedInventory();
  }

  protected updateSavedInventory(): void {
    this.playerService.saveInventory(
      this.ingredients.ingredients,
      this.cauldronContents.ingredients,
      this.potions
    );
  }

  protected restoreFromSavedInventory(): void {
    const saved = this.playerService.loadInventory();
    if (!saved) { return; }
    this.ingredients.add(...saved.ingredients.map(t => new Ingredient(t)));
    this.cauldronContents.add(...saved.cauldron.map(t => new Ingredient(t)));
    this.potions.push(...saved.potions.map(t => new Potion(t)));
  }

  protected transferItem(item: Ingredient, from: IngredientBag, to: IngredientBag): boolean {
    const found = from.removeOne(item);
    if (!found) { return false; }
    to.addOne(item);
    this.updateSavedInventory();
    return true;
  }
}
