import { Component, OnInit } from "@angular/core";
import {InventoryService} from "./inventory.service";
import {Ingredient, IngredientBag, Potion} from '../ingredient/ingredient';

@Component({
  selector: "app-inventory",
  templateUrl: "./inventory.component.html",
  styleUrls: ["./inventory.component.css"]
})
export class InventoryComponent implements OnInit {

  inventory: IngredientBag;

  constructor(public inventoryService: InventoryService) { }

  ngOnInit() {
    this.inventory = this.inventoryService.ingredients;
  }

  public isEmpty() {
    return this.inventory.isEmpty();
  }

  public addContent(item: Ingredient) {
    this.inventoryService.addIngredientToCauldron(item);
  }

  public gatherIngredients() {
    this.inventoryService.gatherIngredients();
  }
}
