import { Injectable } from '@angular/core';
import {Ingredient, Potion} from '../ingredient/ingredient';
import {SavedInventory} from '../inventory/inventory.service';
import {Message} from '../message/message';
import {Observable, Subject} from 'rxjs';

const HOURS_PER_DAY = 8;
const RENT_FREQUENCY = HOURS_PER_DAY * 10;
const BASE_RENT = 5;

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  static readonly INGREDIENTS_STORAGE_KEY = "ingredientsAndEffects";
  static readonly INVENTORY_STORAGE_KEY = "inventory";
  static readonly MESSAGES_STORAGE_KEY = "messages";
  static readonly MONEY_STORAGE_KEY = "money";
  static readonly TIME_STORAGE_KEY = "time";

  public money: number;
  protected hours: number;
  private gameOverEvent: Subject<boolean>;
  private rentPaidEvent: Subject<number>;
  public gameOver: Observable<boolean>;
  public rentPaid: Observable<number>;

  constructor() {
    this.money = this.loadMoney();
    this.hours = this.loadTime();
    this.gameOverEvent = new Subject<boolean>();
    this.gameOver = this.gameOverEvent.asObservable();
    this.rentPaidEvent = new Subject<number>();
    this.rentPaid = this.rentPaidEvent.asObservable();
    // for debugging purposes
    window['playerService'] = this;
  }

  public awardMoney(howMuch: number): void {
    this.money = this.money + howMuch;
    this.saveMoney();
  }

  public rentDueWhen(): number {
    const months = Math.floor(this.hours / RENT_FREQUENCY) + 1;
    return months * RENT_FREQUENCY;
  }

  public rentDueAmount(): number {
    const months = Math.floor(this.hours / RENT_FREQUENCY) + 1;
    return BASE_RENT * Math.pow(2, months);
}

  public daysTilRent(): number {
    return Math.floor((this.rentDueWhen() - this.hours) / HOURS_PER_DAY);
  }

  /*
  I could, and probably should, make a separate service for time, as I can see a lot
  of ways to handle it.  But I'm running out of jam time, so here it goes!
   */
  public dateTimeLong(): string {
    return `Day ${this.day()}, Hour ${this.hour()}`;
  }

  public day(): number {
    return Math.floor(this.hours / HOURS_PER_DAY) + 1;
  }

  public hour(): number {
    return (this.hours % HOURS_PER_DAY) + 1;
  }

  public advanceTime(hours: number): void {
    // Because the rent-to-pay function takes the *current* time to determine when
    // the *next* rent date (and, more importantly, amount) is, we need to check if
    // this advancement of time would cross that line
    if (this.hours + hours >= this.rentDueWhen()) {
      this.payRent();
    }
    this.hours += hours;
    this.saveTime();
    if (this.gameIsOver()) {
      this.gameOverEvent.next(true);
    }
  }

  public payRent(): void {
    const amount = this.rentDueAmount();
    this.awardMoney(-amount);
    this.rentPaidEvent.next(amount);
  }

  public gameIsOver(): boolean {
    return this.money < 0;
  }

  /*
  Similarly, saved/loaded state could be its own service but meh
   */

  public reset(): void {
    localStorage.clear();
  }

  public saveMoney(): void {
    // If only it were this easy to save money
    localStorage.setItem(PlayerService.MONEY_STORAGE_KEY, this.money.toString(10));
  }

  public loadMoney(): number {
    const text = localStorage.getItem(PlayerService.MONEY_STORAGE_KEY);
    if (!text) { return 0; }
    return parseInt(text, 10);
  }

  public saveTime(): void {
    localStorage.setItem(PlayerService.TIME_STORAGE_KEY, this.hours.toString(10));
  }

  public loadTime(): number {
    const text = localStorage.getItem(PlayerService.TIME_STORAGE_KEY);
    if (!text) return 0;
    return parseInt(text, 10);
  }

  public saveIngredientsAndEffects(allIngredients: Ingredient[]): void {
    localStorage.setItem(PlayerService.INGREDIENTS_STORAGE_KEY,
      JSON.stringify(allIngredients));
  }

  public loadIngredientsAndEffects(): Ingredient[] {
    const text = localStorage.getItem(PlayerService.INGREDIENTS_STORAGE_KEY);
    if (text) {
      const templates = JSON.parse(text);
      const ingredients = templates.map(template => {
        const result = new Ingredient(template);
        // TODO: realize effects
        return result;
      });
      return ingredients;
    } else {
      return null;
    }
  }

  public saveInventory(ingredients: Ingredient[], cauldron: Ingredient[], potions: Potion[]) {
    const form: SavedInventory = {
      ingredients,
      cauldron,
      potions
    };
    localStorage.setItem(PlayerService.INVENTORY_STORAGE_KEY,
      JSON.stringify(form));
  }

  public loadInventory(): SavedInventory {
    const text = localStorage.getItem(PlayerService.INVENTORY_STORAGE_KEY);
    if (text) {
      return JSON.parse(text);
    } else {
      return null;
    }
  }

  public saveMessages(messages: Message[]): void {
    localStorage.setItem(PlayerService.MESSAGES_STORAGE_KEY,
      JSON.stringify(messages));
  }

  public loadMessages(): Message[] {
    const text = localStorage.getItem(PlayerService.MESSAGES_STORAGE_KEY);
    if (text) {
      const templates = JSON.parse(text);
      return templates.map(t => new Message(t.content, t.when));
    } else {
      return null;
    }
  }

}
