export class Message {

  constructor(public content: string, public when: string) {
  }
}
