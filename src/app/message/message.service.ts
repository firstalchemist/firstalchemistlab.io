import {Injectable} from '@angular/core';
import {PlayerService} from '../player/player.service';
import {Message} from './message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public messages: Message[];

  constructor(
    public playerService: PlayerService,
  ) {
    this.messages = [];
    this.loadMessages();
    this.playerService.rentPaid.subscribe((amount) => {
      this.add(`Paid $${amount} in wizard fees.`);
    });
  }

  add(message: string): void {
    this.messages.push(new Message(message, this.playerService.dateTimeLong()));
    this.playerService.saveMessages(this.messages);
  }

  clear(): void {
    this.messages.splice(0, this.messages.length);
    this.playerService.saveMessages([]);
  }

  protected loadMessages(): void {
    const messages = this.playerService.loadMessages();
    if (messages) {
      this.messages.push(...messages);
      this.add("Loaded saved game");
    } else {
      this.add("Welcome to the alchemy station!  Start by gathering some ingredients.");
    }
  }
}

