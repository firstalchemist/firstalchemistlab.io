import { Component, OnInit } from '@angular/core';
import {MessageService} from './message.service';
import {Message} from './message';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  public messages: Message[];

  constructor(public messageService: MessageService) { }

  ngOnInit() {
    this.messages = this.messageService.messages;
  }

  clear() {
    this.messageService.clear();
  }

  add(msg: string) {
    this.messageService.add(msg);
  }

}
