import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {InventoryService} from '../inventory/inventory.service';
import {Ingredient, IngredientBag, Potion} from '../ingredient/ingredient';
import {Effect} from '../effects/effect';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MessageService} from '../message/message.service';
import {PlayerService} from '../player/player.service';

@Component({
  selector: "app-cauldron",
  templateUrl: "./cauldron.component.html",
  styleUrls: ["./cauldron.component.css"]
})
export class CauldronComponent implements OnInit {

  contents: IngredientBag;

  lastPotion: Potion;
  lastEffectsLearned: Effect[];
  lastFailureReason: string;

  @ViewChild('potionModal', {static: true})
  private potionsModalTpl: TemplateRef<any>;

  @ViewChild('potionFailed', {static: true})
  private potionFailedTpl: TemplateRef<any>;

  constructor(
    public inventoryService: InventoryService,
    public messageService: MessageService,
    public playerService: PlayerService,
    private modalService: NgbModal,
  ) {

  }

  ngOnInit() {
    this.lastEffectsLearned = [];
    this.lastFailureReason = "You shouldn't see this, this is a bug!";
    this.contents = this.inventoryService.cauldronContents;
  }

  public cauldronImage(): string {
    const descriptor = this.contents.isEmpty() ? "empty" : "stuff";
    return `assets/cauldron_rough_${descriptor}.png`;
  }

  public cauldronDescription(): string {
    const numContents = this.contents.numHolding();
    if (numContents === 0) {
      return "The cauldron is empty";
    }
    return `The cauldron has ${numContents} stuff within it:`;
  }

  public removeContent(content: Ingredient): void {
    this.inventoryService.removeIngredientFromCauldron(content);
  }

  public potionify(): any {
    this.playerService.advanceTime(8);
    this.lastEffectsLearned = [];
    const common: Effect[] = [];
    const effectsSeen = new Set();
    const allEffects: Effect[] = [];
    let rarity = 0;
    for (const ingredient of this.contents.ingredients) {
      if (ingredient.rarity > rarity) { rarity = ingredient.rarity; }
      for (const effect of ingredient.effects) {
        allEffects.push(effect);
        if (effectsSeen.has(effect.name)) {
          common.push(effect);
        }
        effectsSeen.add(effect.name);
      }
    }

    this.contents.clear();

    if (common.length === 0) {
      this.lastFailureReason = "The ingredients had no effects in common.";
      this.awardEffectXp(allEffects, 3);
      this.messageService.add("Failed to make a potion: " + this.lastFailureReason);
      if (!this.playerService.gameIsOver()) {
        this.modalService.open(this.potionFailedTpl, {centered: true});
      }
    } else {
      const effectByName: {[key: string]: Effect} = {};
      for (const effect of common) {
        const lookedUp = effectByName[effect.name];
        if (!lookedUp) {
          effectByName[effect.name] = effect.dup();
        } else {
          lookedUp.combineWithInPlace(effect);
        }
      }
      const potion = new Potion({
        name: Potion.PLACEHOLDER,
        rarity,
        quantity: 1,
        effects: Object.values(effectByName),
      });
      potion.learnEverything();

      this.lastPotion = potion;
      this.inventoryService.addPotion(potion);
      this.awardEffectXp(allEffects, 3, common);
      this.messageService.add("You created: " + potion.displayName());
      if (!this.playerService.gameIsOver()) {
        this.modalService.open(this.potionsModalTpl, {centered: true});
      }
    }
  }

  protected awardEffectXp(allEffects: Effect[], amount: number, learned: Effect[] = []): void {
    const learnedEffectNames = new Set([...learned.map(e => e.name)]);
    this.lastEffectsLearned = allEffects.filter(effect => {
      let actual_amount = learnedEffectNames.has(effect.name) ? Effect.VISIBLE_XP : amount;
      return effect.gainXp(actual_amount);
    });

  }

}
