import { Component, OnInit } from '@angular/core';
import {InventoryService} from '../inventory/inventory.service';
import {Potion} from '../ingredient/ingredient';
import {ALL_FACTIONS, Faction} from '../common/factions';

@Component({
  selector: 'app-marketplace',
  templateUrl: './marketplace.component.html',
  styleUrls: ['./marketplace.component.css']
})
export class MarketplaceComponent implements OnInit {

  potions: Potion[];

  constructor(public inventoryService: InventoryService) { }

  ngOnInit() {
    this.potions = this.inventoryService.potions;
  }

  public factions(): Faction[] {
    return ALL_FACTIONS;
  }

}
