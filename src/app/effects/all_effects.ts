import {SalesCategory} from './effect';
import {Rarity} from '../common/constants';
import {ALL} from 'tslint/lib/rules/completedDocsRule';


export interface EffectCategoryMember {
  name: string;

}

export interface EffectCategory {
  category: string;
  salesCategories: SalesCategory[];  // First is 'positive' cat, second (if present) is 'negative';
  positiveTemplate: string;  // For now these are just prefixes we'll tack on to the names.
  negativeTemplate: string;  // https://squirrelly.js.org/getting-started/installation later
  rarity: Rarity;
  members: EffectCategoryMember[];
}

export const ALL_EFFECTS: EffectCategory[] = [
  {
    category: "Stat-affecting potions",
    salesCategories: [SalesCategory.BOOST, SalesCategory.POISON],
    positiveTemplate: "Enhanced",
    negativeTemplate: "Weakened",
    rarity: Rarity.COMMON,
    members: [
      {name: "Strength"},
      {name: "Constitution"},
      {name: "Dexterity"},
      {name: "Intelligence"},
      {name: "Wisdom"},
      {name: "Charisma"},
    ]
  }
];
