import {PRICE_BY_RARITY, Rarity} from '../common/constants';

export enum SalesCategory {
  BOOST,
  POISON,
  ESOTERIC,
}

export interface EffectTemplate {
  name: string;
  negative: string;
  rarity: Rarity;
  salesCategories: SalesCategory[];

  power?: number;
  xp?: number;
}

export class Effect implements EffectTemplate {
  public static readonly VISIBLE_XP = 10;

  public name: string;
  public negative: string;
  public rarity: number;
  public salesCategories: SalesCategory[];
  public power: number;
  public xp: number;

  constructor(
    template: EffectTemplate
  ) {
    this.power = 0;
    this.xp = 0;
    Object.assign(this, template);
  }

  public dup(): Effect {
    return new Effect(this);
  }

  public displayName(): string {
    if (!this.negative || this.power >= 0) { return this.name; }
    return this.negative;
  }

  public gainXp(gained: number): boolean {
    const oldXp = this.xp;
    this.xp += gained;
    if (oldXp < Effect.VISIBLE_XP && this.xp >= Effect.VISIBLE_XP) {
      return true;
    }
    return false;
  }

  public learn(): void {
    if (this.xp < Effect.VISIBLE_XP) {
      this.xp = Effect.VISIBLE_XP;
    }
  }

  public basePrice(): number {
    return Math.round(PRICE_BY_RARITY[this.rarity] * this.magnitude());
  }

  public salesCategory(): SalesCategory {
    if (this.salesCategories.length === 1) { return this.salesCategories[0]; }
    if (this.power >= 0) { return this.salesCategories[0]; }
    return this.salesCategories[1];
  }

  public magnitude(): number {
    return Math.abs(this.power);
  }

  public displayMagnitude(): string {
    return this.magnitude().toFixed(2);
  }

  public combineWithInPlace(other: EffectTemplate): void {
    this.power += other.power;
  }

  public displayDetails(): string {
    if (this.xp >= Effect.VISIBLE_XP) {
      return `${this.displayName()}, Magnitude: ${this.displayMagnitude()}`;
    } else {
      const pct = Math.round((this.xp / Effect.VISIBLE_XP) * 100);
      return `Unknown effect (${pct}% discovered)`;
    }
  }
}
