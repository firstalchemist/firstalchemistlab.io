import {Component, TemplateRef, ViewChild} from '@angular/core';
import {PlayerService} from './player/player.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'firstalchemist';

  @ViewChild('gameOverModal', {static: true})
  private gameOverModalTpl: TemplateRef<any>;

  constructor(
    public playerService: PlayerService,
    private modalService: NgbModal,
  ) {
    playerService.gameOver.subscribe(over => {
      this.gameOver();
    });
  }

  public gameOver() {
    const options = {
      centered: true,
      beforeDismiss: () => { return false; } // Only close
    }
    this.modalService.open(this.gameOverModalTpl, options).result.then(() => {
      console.log("Whoa");
      this.reset();
    });
  }

  public reset() {
    this.playerService.reset();
    window.location.reload();
  }
}
