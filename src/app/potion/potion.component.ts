import {Component, Input, OnInit} from '@angular/core';
import {Ingredient, Potion} from '../ingredient/ingredient';
import {ALL_FACTIONS, Faction} from '../common/factions';
import {InventoryService} from '../inventory/inventory.service';
import {PlayerService} from '../player/player.service';
import {MessageService} from '../message/message.service';

@Component({
  selector: 'app-potion',
  templateUrl: './potion.component.html',
  styleUrls: ['./potion.component.css']
})
export class PotionComponent implements OnInit {

  @Input() item: Potion;

  constructor(
    public inventoryService: InventoryService,
    public playerService: PlayerService,
    public messageService: MessageService,
  ) {

  }

  ngOnInit() {
  }

  public priceForFaction(faction: Faction): number {
    return faction.priceFor(this.item);
  }

  public factions(): Faction[] {
    return ALL_FACTIONS;
  }

  public sellTo(faction: Faction): void {
    const money = this.priceForFaction(faction);
    this.inventoryService.removePotion(this.item);
    const forWhat = `selling ${this.item.displayName()}`;
    this.messageService.add(`Earned $${money} for ${forWhat}`);
    this.playerService.awardMoney(money);
  }


}
